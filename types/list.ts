import { Stack } from './stack'

export type ForEachCallback<T> = (value: T) => void
export type MapCallback<T> = (value: T) => T
export type ReduceCallback<T, A> = (acc: A, value: T) => A


export interface List<T> extends Stack<T> {
  insert(i: number, value: T): void
  remove(i: number): void

  forEach(callback: ForEachCallback<T>): void
  map(callback: MapCallback<T>): T[]
  reduce<A>(acc: A, callback: ReduceCallback<T, A>): A
}
