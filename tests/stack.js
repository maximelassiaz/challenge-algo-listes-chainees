/**
 * Test suite for Stack implementation
 */
const StackTests = (impl) => {

  test('is empty when created', () => {
    const s = new impl()
    expect(s.isEmpty()).toBeTruthy()
  })

  test('not empty after push', () => {
    const s = new impl()
    s.push('test')
    expect(s.isEmpty()).toBeFalsy()
  })

  test('can pop after push', () => {
    const s = new impl()
    s.push('test')
    expect(s.pop()).toBe('test')
  })

  test('cannot pop when empty', () => {
    const s = new impl()
    expect(() => s.pop()).toThrow('empty stack exception')
  })

  test('cannot pop twice after push', () => {
    const s = new impl()
    s.push('test')
    s.pop()
    expect(() => s.pop()).toThrow('empty stack exception')
  })

  test('can preserve history', () => {
    const s = new impl()
    s.push('test1')
    s.push('test2')
    expect(s.pop()).toBe('test2')
    expect(s.pop()).toBe('test1')
    expect(() => s.pop()).toThrow('empty stack exception')
  })

  test('length is 0 when empty', () => {
    const s = new impl()
    expect(s.length()).toBe(0)
  })

  test('length is 1 after pop', () => {
    const s = new impl()
    s.push('test')
    expect(s.length()).toBe(1)
  })

  test('length is consistent', () => {
    const s = new impl()
    s.push('test')
    s.push('test')
    s.push('test')
    s.pop()
    expect(s.length()).toBe(2)
  })

  test('cannot get negative index', () => {
    const s = new impl()
    expect(() => s.get(-1)).toThrow('invalid negative index')
  })

  test('cannot get when empty', () => {
    const s = new impl()
    expect(() => s.get(0)).toThrow('empty stack exception')
  })

  test('can get head', () => {
    const s = new impl()
    s.push('test1')
    s.push('test2')
    expect(s.get(0)).toBe('test2')
  })

  test('get can search the stack', () => {
    const s = new impl()
    s.push('test1')
    s.push('test2')
    s.push('test3')
    expect(s.get(2)).toBe('test1')
  })

  test('get can overflow', () => {
    const s = new impl()
    s.push('test1')
    expect(() => s.get(1)).toThrow('index overflow')
  })

  test('is empty after clear', () => {
    const s = new impl()
    s.push('test1')
    s.clear()
    expect(s.isEmpty()).toBeTruthy()
  })

}

module.exports = { StackTests }
