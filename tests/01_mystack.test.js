const { StackTests } = require('./stack.js')
const { MyStack } = require('../src/mystack')

describe('MyStack', () => {
  StackTests(MyStack) // Execute stack tests
})
