/**
 * Test suite for Queue implementation
 */
const QueueTests = (impl) => {

  test('cannot popEnd when empty', () => {
    const q = new impl()
    expect(() => q.popEnd()).toThrow('empty queue exception')
  })


  test('can pushEnd when empty', () => {
    const q = new impl()
    q.pushEnd('test')
    expect(q.isEmpty()).toBeFalsy()
  })


  test('can popEnd after pushEnd', () => {
    const q = new impl()
    q.pushEnd('test')
    expect(q.popEnd()).toBe('test')
  })


  test('cannot popEnd after clear', () => {
    const q = new impl()
    q.pushEnd('test')
    q.clear()
    expect(() => q.popEnd()).toThrow('empty queue exception')
  })


  test('cannot popEnd twice after pushEnd', () => {
    const q = new impl()
    q.pushEnd('test')
    expect(q.popEnd()).toBe('test')
    expect(() => q.popEnd()).toThrow('empty queue exception')
  })


  test('can preserve history', () => {
    const q = new impl()
    q.pushEnd('test')
    q.pushEnd('test1')
    q.pushEnd('test2')
    expect(q.popEnd()).toBe('test2')
    expect(q.popEnd()).toBe('test1')
    expect(q.popEnd()).toBe('test')
    expect(() => q.popEnd()).toThrow('empty queue exception')
  })


  test('can pop after pushEnd when empty', () => {
    const q = new impl()
    q.pushEnd('test')
    expect(q.pop()).toBe('test')
    expect(q.isEmpty()).toBeTruthy()
  })


  test('cannot pop after popEnd took the last item', () => {
    const q = new impl()
    q.pushEnd('test')
    expect(q.popEnd()).toBe('test')
    expect(() => q.pop()).toThrow('empty stack exception')
  })


  test('can read forward after pushEnd', () => {
    const q = new impl()
    q.pushEnd('test')
    q.pushEnd('test1')
    q.pushEnd('test2')
    expect(q.pop()).toBe('test')
    expect(q.pop()).toBe('test1')
    expect(q.pop()).toBe('test2')
    expect(() => q.pop()).toThrow('empty stack exception')
    expect(() => q.popEnd()).toThrow('empty queue exception')
  })


  test('can read backward after push', () => {
    const q = new impl()
    q.push('test')
    q.push('test1')
    q.push('test2')
    expect(q.popEnd()).toBe('test')
    expect(q.popEnd()).toBe('test1')
    expect(q.popEnd()).toBe('test2')
    expect(() => q.pop()).toThrow('empty stack exception')
    expect(() => q.popEnd()).toThrow('empty queue exception')
  })


  test('can popEnd after insert at 0', () => {
    const q = new impl()
    q.insert(0, 'test')
    expect(q.popEnd()).toBe('test')
  })


  test('can read backward after insert in the middle', () => {
    const q = new impl()
    q.push('test')
    q.push('test1')
    q.push('test2')
    q.insert(1, 'test3')
    expect(q.popEnd()).toBe('test')
    expect(q.popEnd()).toBe('test1')
    expect(q.popEnd()).toBe('test3')
    expect(q.popEnd()).toBe('test2')
    expect(() => q.popEnd()).toThrow('empty queue exception')
  })


  test('can popEnd after insert at the end', () => {
    const q = new impl()
    q.push('test1')
    q.push('test2')
    q.push('test3')
    q.insert(3, 'test')
    expect(q.popEnd()).toBe('test')
  })


  test('can remove at 0 after pushEnd', () => {
    const q = new impl()
    q.pushEnd('test')
    q.remove(0)
    expect(q.isEmpty()).toBeTruthy()
  })


  test('cannot popEnd after removing the last remaining item', () => {
    const q = new impl()
    q.pushEnd('test')
    q.remove(0)
    expect(() => q.popEnd()).toThrow('empty queue exception')
  })


  test('can remove the last item', () => {
    const q = new impl()
    q.pushEnd('test')
    q.pushEnd('test2')
    q.pushEnd('test3')
    q.remove(2)
    expect(q.length()).toBe(2)
    expect(q.popEnd()).toBe('test2')
    expect(q.popEnd()).toBe('test')
    expect(() => q.popEnd()).toThrow('empty queue exception')
  })


  test('remove preserve reverse history', () => {
    const q = new impl()
    q.pushEnd('test1')
    q.pushEnd('test2')
    q.pushEnd('test3')
    q.remove(1)
    expect(q.popEnd()).toBe('test3')
    expect(q.popEnd()).toBe('test1')
    expect(() => q.popEnd()).toThrow('empty queue exception')
  })


  test('forEachR iterates over the list in reverse order', () => {
    const q = new impl()
    q.push('test1')
    q.push('test2')
    q.push('test3')

    const content = []
    q.forEachR(v => content.push(v))
    expect(content).toStrictEqual(['test1', 'test2', 'test3'])
  })


  test('mapR iterates the list in reverse order to transform each item', () => {
    const q = new impl()
    q.push('test1')
    q.push('test2')
    q.push('test3')

    const content = []
    const res = q.mapR(v => v.toUpperCase())
    expect(res).toStrictEqual(['TEST1', 'TEST2', 'TEST3'])
  })


  test('reduceR accumulates while iterating the list in reverse order', () => {
    const q = new impl()
    q.push('1')
    q.push('2')
    q.push('3')

    let acc = 0
    const content = []
    const res = q.reduceR(acc, (acc, v) => {
      content.push(v)
      return acc + parseInt(v)
    })
    expect(res).toBe(6)
    expect(content).toStrictEqual(['1', '2', '3'])
  })


}

module.exports = { QueueTests }
