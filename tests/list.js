/**
 * Test suite for List implementation
 */
const ListTests = (impl) => {

  test('cannot insert at negative index', () => {
    const s = new impl()
    expect(() => s.insert(-1, 'test')).toThrow('invalid negative index')
  })

  test('can insert at start on empty list', () => {
    const l = new impl()
    l.insert(0, 'test')
    expect(l.pop()).toBe('test')
  })

  test('can insert at start', () => {
    const l = new impl()
    l.push('test1')
    l.insert(0, 'test')
    expect(l.pop()).toBe('test')
    expect(l.pop()).toBe('test1')
  })

  test('can insert in the middle', () => {
    const l = new impl()
    l.push('test3')
    l.push('test2')
    l.push('test1')
    l.insert(2, 'test')
    expect(l.get(2)).toBe('test')
    expect(l.get(3)).toBe('test3')
    expect(l.length()).toBe(4)
  })

  test('can insert at the end', () => {
    const l = new impl()
    l.push('test1')
    l.push('test2')
    l.insert(2, 'test')
    expect(l.get(2)).toBe('test')
    expect(l.length()).toBe(3)
  })

  test('insert can overflow', () => {
    const l = new impl()
    l.push('test')
    expect(() => l.insert(2, 'test')).toThrow('index overflow')
  })

  test('cannot remove from empty list', () => {
    const l = new impl()
    expect(() => l.remove(0)).toThrow('empty list exception')
  })

  test('can remove the last remaining item', () => {
    const l = new impl()
    l.push('test')
    l.remove(0)
    expect(l.isEmpty()).toBeTruthy()
  })

  test('can remove head', () => {
    const l = new impl()
    l.push('test1')
    l.push('test2')
    l.remove(0)
    expect(l.length()).toBe(1)
    expect(l.pop()).toBe('test1')
  })

  test('can remove in the middle', () => {
    const l = new impl()
    l.push('test1')
    l.push('test2')
    l.push('test3')
    l.remove(1)
    expect(l.length()).toBe(2)
    expect(l.pop()).toBe('test3')
    expect(l.pop()).toBe('test1')
  })

  test('can remove last item', () => {
    const l = new impl()
    l.push('test1')
    l.push('test2')
    l.push('test3')
    l.remove(2)
    expect(l.length()).toBe(2)
    expect(l.pop()).toBe('test3')
    expect(l.pop()).toBe('test2')
  })

  test('remove can overflow', () => {
    const l = new impl()
    l.push('test1')
    l.push('test2')
    expect(() => l.remove(2)).toThrow('index overflow')
  })

  test('forEach iterate over the list', () => {
    const l = new impl()
    l.push('test1')
    l.push('test2')
    l.push('test3')

    const content = []
    l.forEach(v => content.push(v))
    expect(content).toStrictEqual(['test3', 'test2', 'test1'])
  })

  test('map iterate over the list to transform each item', () => {
    const l = new impl()
    l.push('test1')
    l.push('test2')
    l.push('test3')

    const content = []
    const res = l.map(v => v.toUpperCase())
    expect(res).toStrictEqual(['TEST3', 'TEST2', 'TEST1'])
  })

  test('reduce accumulates while iterating over the list', () => {
    const l = new impl()
    l.push('1')
    l.push('2')
    l.push('3')

    let acc = 0
    const res = l.reduce(acc, (acc, v) => {
      return acc + parseInt(v)
    })
    expect(res).toBe(6)
  })

}

module.exports = { ListTests }
