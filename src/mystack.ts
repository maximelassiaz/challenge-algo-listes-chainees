import { Stack } from '@stack'


export interface Link<T> {
  value: T
  next: Link<T> | null
}


export class MyStack<T> implements Stack<T> {

  protected head: Link<T> | null


  constructor() {
    this.head = null
  }


  public isEmpty(): boolean {
    throw new Error('not yet implemented')
  }


  public push(value: T): void {
    throw new Error('not yet implemented')
  }


  public pop(): T {
    throw new Error('not yet implemented')
  }


  public length(): number {
    throw new Error('not yet implemented')
  }


  public get(i: number): T {
    throw new Error('not yet implemented')
  }


  public clear(): void {
    throw new Error('not yet implemented')
  }

}